package academy.devdojo.springboot2.service.impl;

import academy.devdojo.springboot2.domain.Anime;

import academy.devdojo.springboot2.exception.BadRequestException;
import academy.devdojo.springboot2.mapper.AnimeMapper;
import academy.devdojo.springboot2.repository.AnimeRepository;
import academy.devdojo.springboot2.requests.AnimePostRequestBody;
import academy.devdojo.springboot2.requests.AnimePutRequestBody;
import academy.devdojo.springboot2.service.AnimeService;
import academy.devdojo.springboot2.util.DateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;


@Service
@RequiredArgsConstructor
@Log4j2
public class AnimeServiceImpl implements AnimeService {

    private final DateUtil dateUtil;
    private final AnimeRepository repository;

    @Override
    public Page<Anime> pageList(Pageable pageable) {
        log.info(dateUtil.formatLocalDateTimeToDatebaseStyle(LocalDateTime.now()));
        return repository.findAll(pageable);
    }

    @Override
    public List<Anime> listAll() {
        return repository.findAll();
    }

    @Override
    public Anime findByIdOrThrowBadRequestException(Long id) {
        log.info(dateUtil.formatLocalDateTimeToDatebaseStyle(LocalDateTime.now()));
        return repository.findById(id)
                .orElseThrow(() -> new BadRequestException("Anime not found"));
    }

    @Transactional
    @Override
    public Anime save(AnimePostRequestBody animePostRequestBody) {
        log.info(dateUtil.formatLocalDateTimeToDatebaseStyle(LocalDateTime.now()));
        return repository.save(AnimeMapper.INSTANCE.toAnime(animePostRequestBody));
    }

    @Override
    public void delete(Long id) {
        repository.delete(findByIdOrThrowBadRequestException(id));
    }

    @Override
    public void update(AnimePutRequestBody animePutRequestBody) {
        Anime savadAnime = findByIdOrThrowBadRequestException(animePutRequestBody.getId());
        Anime anime = AnimeMapper.INSTANCE.toAnime(animePutRequestBody);
        anime.setId(savadAnime.getId());
        repository.save(anime);
    }

    @Override
    public List<Anime> findByName(String name) {
        return repository.findByName(name);
    }

}
