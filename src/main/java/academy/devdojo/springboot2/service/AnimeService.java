package academy.devdojo.springboot2.service;

import academy.devdojo.springboot2.domain.Anime;
import academy.devdojo.springboot2.requests.AnimePostRequestBody;
import academy.devdojo.springboot2.requests.AnimePutRequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AnimeService {

    Page<Anime> pageList(Pageable pageable);

    List<Anime> listAll();

    Anime findByIdOrThrowBadRequestException(Long id);

    Anime save(AnimePostRequestBody anime);

    void delete(Long id);

    void update(AnimePutRequestBody anime);

    List<Anime> findByName(String name);
}
