package academy.devdojo.springboot2.client;


import academy.devdojo.springboot2.configurer.RestTemplateConfig;
import academy.devdojo.springboot2.domain.Anime;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Log4j2
public class SpringClient {

    public static void main(String[] args) {

        ResponseEntity<Anime> entity = new RestTemplate().getForEntity("http://localhost:8080/animes/{id}",
                Anime.class, 3);
        log.info(entity);


        int index = 5;
        Anime object = new RestTemplate().getForObject("http://localhost:8080/animes/{id}", Anime.class, index);
        log.info(object);


        Anime[] animes = new RestTemplate().getForObject("http://localhost:8080/animes/all", Anime[].class);
        log.info(Arrays.toString(animes));

        // modo correto
        ResponseEntity<List<Anime>> exchange = new RestTemplate().exchange("http://localhost:8080/animes/all", HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {
                });
        log.info(exchange.getBody());


//        Anime kingdom = Anime.builder().name("kingdom").build();
//        Anime animeSaved = new RestTemplate().postForObject("http://localhost:8080/animes", kingdom, Anime.class);
//        log.info(animeSaved);

//        Anime samurai = Anime.builder().name("Samurai Chamloo").build();
//        ResponseEntity<Anime> samuraiSaved = new RestTemplate().exchange("http://localhost:8080/animes", HttpMethod.POST, new HttpEntity<>(samurai), Anime.class);
//        log.info(samuraiSaved.getBody());

//        Anime samuraiShowdown = Anime.builder().name("Samurai Showdown").build();
//        ResponseEntity<Anime> samuraiShowdownSaved = new RestTemplate().exchange("http://localhost:8080/animes", HttpMethod.POST, new HttpEntity<>(samuraiShowdown, httpHeaders()), Anime.class);
//        log.info(samuraiShowdownSaved);
//
//
//        Anime animeUpdate = samuraiShowdownSaved.getBody();
//        animeUpdate.setName("Samurai Showdown 2");
//        ResponseEntity<Void> animeUp = new RestTemplate().exchange("http://localhost:8080/animes", HttpMethod.PUT, new HttpEntity<>(animeUpdate, httpHeaders()), Void.class);
//        log.info(animeUp);

        ResponseEntity<Void> animeToBeDeleted = new RestTemplate().exchange("http://localhost:8080/animes/{id}", HttpMethod.DELETE, null, Void.class, 15);
        log.info(animeToBeDeleted);

    }
    private static HttpHeaders httpHeaders(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }


 }

