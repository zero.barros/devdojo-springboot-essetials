package academy.devdojo.springboot2.service.impl;

import academy.devdojo.springboot2.domain.Anime;
import academy.devdojo.springboot2.exception.BadRequestException;
import academy.devdojo.springboot2.repository.AnimeRepository;
import academy.devdojo.springboot2.service.AnimeService;
import academy.devdojo.springboot2.util.AnimeCreator;
import academy.devdojo.springboot2.util.AnimePostRequestBodyCreator;
import academy.devdojo.springboot2.util.AnimePutRequestBodyCreator;
import academy.devdojo.springboot2.util.DateUtil;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
class AnimeServiceImplTest {


    @InjectMocks
    private AnimeServiceImpl animeService;

    @Mock
    private AnimeRepository animeRepositoryMock;


    @Mock
    private DateUtil dateUtil;

    @BeforeEach
    void setup(){
        PageImpl<Anime> animePage = new PageImpl<>(List.of(AnimeCreator.createValidAnime()));
        BDDMockito.when(animeRepositoryMock.findAll(ArgumentMatchers.any(PageRequest.class)))
                .thenReturn(animePage);

        BDDMockito.when(animeRepositoryMock.findAll())
                .thenReturn(List.of(AnimeCreator.createValidAnime()));

        BDDMockito.when(animeRepositoryMock.findById(ArgumentMatchers.anyLong()))
                .thenReturn(Optional.of(AnimeCreator.createValidAnime()));

        BDDMockito.when(animeRepositoryMock.findByName(ArgumentMatchers.anyString()))
                .thenReturn(List.of(AnimeCreator.createValidAnime()));

        BDDMockito.when(animeRepositoryMock.save(ArgumentMatchers.any()))
                .thenReturn(AnimeCreator.createValidAnime());

//        BDDMockito.doNothing().when(animeRepositoryMock).save(ArgumentMatchers.any());

        BDDMockito.doNothing().when(animeRepositoryMock).delete(ArgumentMatchers.any());

    }

    @Test
    @DisplayName("pageList returns list of anime inside page object when successful")
    public void pageList_ReturnsListOfAnimesInsidePageObject_WhenSussecceful(){

        String expectedName = AnimeCreator.createValidAnime().getName();

        Page<Anime> animePage = animeService.pageList(PageRequest.of(1, 1));

        Assertions.assertThat(animePage).isNotNull();

        Assertions.assertThat(animePage.toList())
                .isNotEmpty()
                .hasSize(1);

        Assertions.assertThat(animePage.toList().get(0).getName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("ListAll returns list of anime when successful")
    public void list_ReturnsListOfAnimes_WhenSussecceful(){

        String expectedName = AnimeCreator.createValidAnime().getName();

        List<Anime> animeList = animeService.listAll();

        Assertions.assertThat(animeList)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1);

        Assertions.assertThat(animeList.get(0).getName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("findById resturs anime when successful")
    public void findById_ReturnsAnimes_WhenSussecceful(){

        long expectedId = AnimeCreator.createValidAnime().getId();

        Anime anime = animeService.findByIdOrThrowBadRequestException(AnimeCreator.createValidAnime().getId());

        Assertions.assertThat(anime).isNotNull();

        Assertions.assertThat(anime.getId())
                .isEqualTo(expectedId)
                .isNotNull();
    }

    @Test
    @DisplayName("findById resturs throw BadRequestException when anime is not found")
    public void findById_ReturnsThrow_BadRequestException_WhenAnimeIsNotFound(){
        BDDMockito.when(animeRepositoryMock.findById(ArgumentMatchers.anyLong()))
                .thenReturn(Optional.empty());

        //verifique que
//        Assertions.assertThatThrownBy(() -> this.animeService.findByIdOrThrowBadRequestException(1l))
//                .isInstanceOf(BadRequestException.class);
        //Ou
        Assertions.assertThatExceptionOfType(BadRequestException.class)
                .isThrownBy(() -> this.animeService.findByIdOrThrowBadRequestException(1L))
                .withMessageContaining("Anime not found");
    }

    @Test
    @DisplayName("findByName resturs anime when successful")
    public void findByName_ReturnsListOfAnimes_WhenSussecceful(){

        long expectedId = AnimeCreator.createValidAnime().getId();

        String expectedName = AnimeCreator.createValidAnime().getName();

        List<Anime> animes = animeService.findByName(AnimeCreator.createValidAnime().getName());

        Assertions.assertThat(animes)
                .isNotEmpty()
                .isNotNull()
                .hasSize(1);

        Assertions.assertThat(animes.get(0).getId())
                .isEqualTo(expectedId)
                .isNotNull();

        Assertions.assertThat(animes.get(0).getName())
                .isEqualTo(expectedName)
                .isNotNull();
    }

    @Test
    @DisplayName("findByName resturs empty list of anime when anime is not found")
    public void findByName_ReturnsEmptyListOfAnimes_WhenAnimeIsNotFound(){

        BDDMockito.when(animeRepositoryMock.findByName(ArgumentMatchers.anyString()))
                .thenReturn(Collections.emptyList());

        List<Anime> animes = animeService.findByName(AnimeCreator.createValidAnime().getName());

        Assertions.assertThat(animes)
                .isNotNull()
                .isEmpty();

    }

    @Test
    @DisplayName("save persists anime when successful")
    public void save_PersistsAnimes_WhenSussecceful(){

        Anime anime = animeService.save(AnimePostRequestBodyCreator.createAnimeToBeSaved());

        Assertions.assertThat(anime).isNotNull().isEqualTo(AnimeCreator.createValidAnime());
    }

    @Test
    @DisplayName("update persists anime when successful")
    public void update_PersistsAnimes_WhenSussecceful() {

        Assertions.assertThatCode(() -> animeService.update(AnimePutRequestBodyCreator.createAnimePutRequestBody()))
                .doesNotThrowAnyException();
    }

    @Test
    @DisplayName("delete anime when successful")
    public void delete_Animes_WhenSussecceful() {

        Assertions.assertThatCode(() -> animeService.delete(AnimeCreator.createValidAnime().getId()))
                .doesNotThrowAnyException();

    }
}