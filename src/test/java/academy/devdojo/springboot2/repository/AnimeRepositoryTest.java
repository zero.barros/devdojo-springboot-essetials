package academy.devdojo.springboot2.repository;

import academy.devdojo.springboot2.domain.Anime;

import academy.devdojo.springboot2.util.AnimeCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@DataJpaTest
@DisplayName("Tests for Anime Repository")
class AnimeRepositoryTest {

    @Autowired
    private AnimeRepository animeRepository;

    @Test
    @DisplayName("Save persists anime when successful")
    void save_PersistAnime_WhenSuccessful(){
        Anime animeToBeSavad = AnimeCreator.createAnimeToBeSaved();
        Anime animeSavad = this.animeRepository.save(animeToBeSavad);
        //verifique que
        Assertions.assertThat(animeSavad).isNotNull();
        Assertions.assertThat(animeSavad.getId()).isNotNull();
        Assertions.assertThat(animeSavad.getName()).isEqualTo(animeToBeSavad.getName());
    }

    @Test
    @DisplayName("Save updates anime when successful")
    void save_UpdatesAnime_WhenSuccessful(){
        Anime animeToBeSavad = AnimeCreator.createAnimeToBeSaved();
        Anime animeSavad = this.animeRepository.save(animeToBeSavad);
        animeSavad.setName("Overlord");
        Anime animeUpdated = this.animeRepository.save(animeToBeSavad);
        //verifique que
        Assertions.assertThat(animeUpdated).isNotNull();
        Assertions.assertThat(animeUpdated.getId()).isNotNull();
        Assertions.assertThat(animeUpdated.getName()).isEqualTo(animeSavad.getName());
    }

    @Test
    @DisplayName("Delete removes anime when successful")
    void delete_RemovesAnime_WhenSuccessful(){
        Anime animeToBeSavad = AnimeCreator.createAnimeToBeSaved();
        Anime animeSavad = this.animeRepository.save(animeToBeSavad);
        this.animeRepository.delete(animeSavad);
        Optional<Anime> animeOpt = this.animeRepository.findById(animeSavad.getId());
        Assertions.assertThat(animeOpt).isEmpty();
    }

    @Test
    @DisplayName("Find by Name returns list of anime when successful")
    void findByName_ReturnListOfAnime_WhenSuccessful(){
        Anime animeToBeSavad = AnimeCreator.createAnimeToBeSaved();
        Anime animeSavad = this.animeRepository.save(animeToBeSavad);
        String name = animeSavad.getName();
        List<Anime> animes = this.animeRepository.findByName(name);
        Assertions.assertThat(animes)
                .isNotEmpty()
                .contains(animeSavad);
    }

    @Test
    @DisplayName("Find by Name returns empty list of anime when no anime is found")
    void findByName_ReturnEmptyList_WhenAnimeIsNoFound(){
        List<Anime> animes = this.animeRepository.findByName("");
        Assertions.assertThat(animes).isEmpty();
    }

    @Test
    @DisplayName("Save throw ConstraintViolationException when name is empty")
    void save_ThrowConstraintViolationException_WhenNameIsEmpty(){
        Anime anime = new Anime();
        //verifique que
//        Assertions.assertThatThrownBy(() -> this.animeRepository.save(anime))
//                .isInstanceOf(ConstraintViolationException.class);
        //Ou
        Assertions.assertThatExceptionOfType(ConstraintViolationException.class)
                .isThrownBy(() -> this.animeRepository.save(anime))
                .withMessageContaining("The anime name cannot be empty");
    }
}