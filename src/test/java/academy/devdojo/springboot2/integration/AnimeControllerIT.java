package academy.devdojo.springboot2.integration;


import academy.devdojo.springboot2.domain.Anime;
import academy.devdojo.springboot2.repository.AnimeRepository;
import academy.devdojo.springboot2.requests.AnimePostRequestBody;
import academy.devdojo.springboot2.util.AnimeCreator;
import academy.devdojo.springboot2.util.AnimePostRequestBodyCreator;
import academy.devdojo.springboot2.wrapper.PageableResponse;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class AnimeControllerIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

//    @LocalServerPort
//    private int port;

    @Autowired
    private AnimeRepository animeRepository;

    @Test
    @DisplayName("List returns list of anime inside page object when successful")
    public void list_ReturnsListOfAnimesInsidePageObject_WhenSussecceful(){

        Anime animeSaved = animeRepository.save(AnimeCreator.createAnimeToBeSaved());

        String expectedName = animeSaved.getName();

        PageableResponse<Anime> anime = testRestTemplate.exchange("/animes", HttpMethod.GET, null,
                new ParameterizedTypeReference<PageableResponse<Anime>>() {
                }).getBody();

        Assertions.assertThat(anime).isNotNull();

        Assertions.assertThat(anime.toList())
                .isNotEmpty()
                .hasSize(1);

        Assertions.assertThat(anime.toList().get(0).getName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("ListAll returns list of anime when successful")
    public void list_ReturnsListOfAnimes_WhenSussecceful(){

        Anime animeSaved = animeRepository.save(AnimeCreator.createAnimeToBeSaved());

        String expectedName = animeSaved.getName();

        List<Anime> animes = testRestTemplate.exchange("/animes/all", HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Anime>>() {
                }).getBody();

        Assertions.assertThat(animes)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1);

        Assertions.assertThat(animes.get(0).getName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("findById resturs anime when successful")
    public void findById_ReturnsAnimes_WhenSussecceful(){
        Anime animeSaved = animeRepository.save(AnimeCreator.createAnimeToBeSaved());

        long expectedId = animeSaved.getId();

        Anime anime = testRestTemplate.getForObject("/animes/{id}", Anime.class, expectedId);

        Assertions.assertThat(anime).isNotNull();

        Assertions.assertThat(anime.getId())
                .isEqualTo(expectedId)
                .isNotNull();
    }

    @Test
    @DisplayName("findByName resturs anime when successful")
    public void findByName_ReturnsListOfAnimes_WhenSussecceful(){

        Anime animeSaved = animeRepository.save(AnimeCreator.createAnimeToBeSaved());

        long expectedId = animeSaved.getId();

        String expectedName = animeSaved.getName();

        String url = String.format("/animes/find?name=%s", expectedName);

        List<Anime> animes = testRestTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Anime>>() {
                }).getBody();

        Assertions.assertThat(animes)
                .isNotEmpty()
                .isNotNull()
                .hasSize(1);

        Assertions.assertThat(animes.get(0).getId())
                .isEqualTo(expectedId)
                .isNotNull();

        Assertions.assertThat(animes.get(0).getName())
                .isEqualTo(expectedName)
                .isNotNull();
    }

    @Test
    @DisplayName("findByName resturs empty list of anime when anime is not found")
    public void findByName_ReturnsEmptyListOfAnimes_WhenAnimeIsNotFound(){

        List<Anime> animes = testRestTemplate.exchange("/animes/find?name=dbz", HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Anime>>() {
                }).getBody();

        Assertions.assertThat(animes)
                .isNotNull()
                .isEmpty();
    }

    @Test
    @DisplayName("save persists anime when successful")
    public void save_PersistsAnimes_WhenSussecceful(){
        AnimePostRequestBody animeToBeSaved = AnimePostRequestBodyCreator.createAnimeToBeSaved();

        ResponseEntity<Anime> animeResponseEntity = testRestTemplate
                .postForEntity("/animes", animeToBeSaved, Anime.class);

        Assertions.assertThat(animeResponseEntity).isNotNull();

        Assertions.assertThat(animeResponseEntity
                .getStatusCode())
                .isEqualTo(HttpStatus.CREATED);

        Assertions.assertThat(animeResponseEntity.getBody()).isNotNull();

        Assertions.assertThat(animeResponseEntity.getBody().getId()).isNotNull();
    }

    @Test
    @DisplayName("update persists anime when successful")
    public void update_PersistsAnimes_WhenSussecceful() {

        Anime animeSaved = animeRepository.save(AnimeCreator.createAnimeToBeSaved());

        animeSaved.setName("xas");

        ResponseEntity<Void> animeResposeEntity = testRestTemplate.exchange("/animes", HttpMethod.PUT, new HttpEntity<>(animeSaved), Void.class);


        Assertions.assertThat(animeResposeEntity).isNotNull();

        Assertions.assertThat(animeResposeEntity
                .getStatusCode())
                .isEqualTo(HttpStatus.NO_CONTENT);

    }

    @Test
    @DisplayName("delete anime when successful")
    public void delete_Animes_WhenSussecceful() {

        Anime animeSaved = animeRepository.save(AnimeCreator.createAnimeToBeSaved());

        ResponseEntity<Void> animeResposeEntity = testRestTemplate.exchange("/animes/{id}", HttpMethod.DELETE, null, Void.class, animeSaved.getId());

        Assertions.assertThat(animeResposeEntity).isNotNull();

        Assertions.assertThat(animeResposeEntity
                .getStatusCode())
                .isEqualTo(HttpStatus.NO_CONTENT);


    }


}
